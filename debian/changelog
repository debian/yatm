yatm (0.9-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 0.6-1.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 13:21:08 +0100

yatm (0.9-4) unstable; urgency=medium

  * QA upload.
  * debian/control:
      - Added pkg-config in Build-Depends field to fix FTBFS.
        (Closes: #1020166)
      - Bumped Standards-Version to 4.6.1.
  * debian/copyright: updated years in packaging block.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Wed, 28 Sep 2022 20:20:14 -0300

yatm (0.9-3) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added Rules-Requires-Root: no.
      - Added Vcs-Browser field to source stanza.
      - Build-Depends field organized alphabetically.
      - Bumped Standards-Version to 4.6.0.
      - Updated Vcs-Git field to Salsa repository.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/gbp.conf: removed, not needed.
  * debian/patches/010_fix-spelling-error.patch: added.
  * debian/rules: added DEB_BUILD_MAINT_OPTIONS variable to improve GCC
    hardening.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/upstream/metadata: created.
  * debian/watch: fixed.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Fri, 22 Oct 2021 15:48:16 -0300

yatm (0.9-2) unstable; urgency=medium

  * QA upload.
  * Add a Homepage field.
  * Add a watch file.
  * Bump DH level to 10.
  * Bump Standards-Version to 3.9.8.
  * Set Debian QA Group as maintainer. (see #840875)

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Fri, 28 Oct 2016 07:32:35 -0500

yatm (0.9-1) unstable; urgency=low

  * New release which fixes a libSoundTouch incompatibility
    (Closes: Bug#793122).

 -- Mario Lang <mlang@debian.org>  Tue, 21 Jul 2015 16:53:33 +0200

yatm (0.8-2) unstable; urgency=low

  * Build-Depend on libsoundtouch-dev, instead of libsoundtouch0-dev
    (Closes: Bug#793071).
  * Use source format "3.0 (quilt)".
  * Use debhelper 9.
  * Update Standards-Version to 3.9.6.

 -- Mario Lang <mlang@debian.org>  Tue, 21 Jul 2015 11:45:06 +0200

yatm (0.8-1) unstable; urgency=low

  * New upstream release (Closes: Bug#611530, Bug#697947, Bug#704894).
  * Replace cdbs with debhelper.

 -- Mario Lang <mlang@debian.org>  Mon, 16 Sep 2013 18:48:44 +0200

yatm (0.6-1) unstable; urgency=low

  * Initial Release.

 -- Mario Lang <mlang@debian.org>  Sun, 25 Nov 2007 21:40:59 +0200
